import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  factores: IFactor[] = [
    {
      id: '222',
      activo: false,
      descripcion: '',
      nombre: 'factor 1',
      descripciones: [
        'desc 1',
        'desc 2',
        'desc 3',
        'desc 4',
        'desc 5',
        'desc 6',
      ]
    },
    {
      id: '333',
      activo: false,
      descripcion: '',
      nombre: 'factor 2',
      descripciones: [
        'desc 7',
        'desc 8',
        'desc 9',
        'desc 10',
        'desc 11',
        'desc 12',
      ]
    }
  ];

  puntoId = '111';
  form: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder
  ) {}

  get campoFactores(): FormArray {
    return this.form.get('factores') as FormArray;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      puntoId: this.fb.control(this.puntoId, [Validators.required]),
      factores: this.fb.array([])
    });
    this.factores.forEach((factor) => {
      this.addFactor(factor.id, factor.descripcion);
    });
  }

  addFactor(factorId: string, descripcion: string, activo = false) {
    (this.form.get('factores') as FormArray).push(
      this.fb.group({
        activo: this.fb.control(activo, []),
        factorId: this.fb.control(factorId, []),
        descripciones: this.fb.array([]),
      })
    );
  }

  seleccionarFactor(event: {descripcion: string; factorId: number}) {
    console.log('selecciona factor',event);
    const factores: FormArray = this.form.get('factores') as FormArray;
    const descripciones = (factores.controls[event.factorId].get('descripciones') as FormArray);
    descripciones.push(this.adicionarDescripcion({id: '', valor: event.descripcion}));
  }

  adicionarDescripcion(descripcion: {id: string; valor: string}): FormGroup {
    const grupoDescripcion = this.fb.group(
      {
        id: this.fb.control(descripcion.id),
        valor: this.fb.control(descripcion.valor, [Validators.required])
      }
    );
    return grupoDescripcion;
  }

  removerDescripcion(factorIdx: number, descripcionIdx: number) {
    const factores: FormArray = this.form.get('factores') as FormArray;
    const descripciones = (factores.controls[factorIdx].get('descripciones') as FormArray);
    descripciones.removeAt(descripcionIdx)
  }

  getDescripciones(i: number): FormArray {
    const factores: FormArray = this.form.get('factores') as FormArray;
    const descripciones = (factores.controls[i].get('descripciones') as FormArray);
    return descripciones;
  }

  addOtro(nombre: string) {
    console.log('otro');
    this.factores.push({
      id: 'otro',
      activo: true,
      descripcion: '',
      descripciones: [],
      nombre
    });
    const factor = this.factores[this.factores.length - 1];
    this.addFactor(factor.id, factor.descripcion, factor.activo);
  }
}

interface IFactor {
  id: string;
  nombre: string
  descripcion: string;
  descripciones: string[];
  activo: boolean;
}